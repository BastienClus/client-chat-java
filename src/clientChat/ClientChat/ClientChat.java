package clientChat.ClientChat;

import clientChat.IOCommandes.IOCommandes;

import java.io.IOException;
import java.net.Socket;

public class ClientChat {

    private IOCommandes ioCommandes;

    public ClientChat() {
        Socket socket = null;
        try {
            socket = new Socket("172.16.1.64", 5999);
        } catch (
                IOException e) {
            e.printStackTrace();
        }

        IOCommandes ioCommandes = new IOCommandes(socket);
    }


    public void dialogueConsole() {

        String pseudo = "";
        int choix;

        ioCommandes.ecrireEcran("Bienvenue sur TPChat, veuillez vous identifier");
        do{
            ioCommandes.ecrireEcran("Entrez votre pseudo");
            pseudo = ioCommandes.lireEcran();
            ioCommandes.ecrireReseau("USER:<" + pseudo + ">");
        }
        while(!ioCommandes.lireReseau().startsWith("OK"));


        do{
            ioCommandes.ecrireEcran("Que voulez vous faire ? ");
            ioCommandes.ecrireEcran("1 -> Voir liste users");
            ioCommandes.ecrireEcran("2 -> Relever les messages");
            ioCommandes.ecrireEcran("3 -> Envoyer un message general");
            ioCommandes.ecrireEcran("4 -> Envoyer un message personnel");
            ioCommandes.ecrireEcran("5 -> Quitter");
            choix = Integer.parseInt(ioCommandes.lireEcran());
        } while(choix<= 5 && choix >= 1);

        if(choix == 1){
            commandeUsers();
        } else if(choix == 2){
            commandeMessages();
        } else if(choix == 3){
            commandeEnvoiGeneral();
        } else if(choix == 4){
            commandeEnvoiPrive();
        } else if(choix == 5){
            commandeQuit();
        }

        /*String res = ioCommandes.lireEcran();


        while (!res.equals("QUIT")) {
            if(res.startsWith("SEND:") && !res.isEmpty()) {

                    ioCommandes.ecrireReseau(res);
                    ioCommandes.lireReseau();
            }
            if(res.equals("MESSAGES") && !res.isEmpty()){
                ioCommandes.ecrireReseau(res);
                String firstLine = ioCommandes.lireReseau();
                int nbrLine = Integer.parseInt(firstLine.split(" ")[0].split(":")[1]);
                for (int i = 0; i < nbrLine; i++) {
                    ioCommandes.ecrireEcran(ioCommandes.lireReseau());
                }
            }
        };*/
    }

    public void commandeUsers(){
        ioCommandes.ecrireReseau("USERS");
        ioCommandes.lireReseau();
    }

    public void commandeMessages(){
        ioCommandes.ecrireReseau("MESSAGES");
        ioCommandes.lireReseau();
    }

    public void commandeEnvoiGeneral() {
        ioCommandes.ecrireEcran("Texte a envoyer");
        String msg = ioCommandes.lireEcran();
        ioCommandes.ecrireReseau("SEND:" + msg);
        ioCommandes.lireReseau();
    }

    public void commandeEnvoiPrive(){
        ioCommandes.ecrireEcran("Texte a envoyer");
        String msg = ioCommandes.lireEcran();
        ioCommandes.ecrireEcran("Destinataire : ");
        String destinataire = ioCommandes.lireEcran();
        ioCommandes.ecrireReseau("SEND " + msg + " " + destinataire);
        ioCommandes.lireReseau();
    }

    public void commandeQuit(){
        ioCommandes.ecrireReseau("QUIT");
    }
}
