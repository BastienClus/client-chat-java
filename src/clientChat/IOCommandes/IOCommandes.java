package clientChat.IOCommandes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class IOCommandes {
    private BufferedReader lectureEcran;
    private PrintStream ecritureEcran;
    private Socket reseau;
    private BufferedReader lectureReseau;
    private PrintStream ecritureReseau;


    public IOCommandes(Socket socket) {
        lectureEcran = new BufferedReader(new InputStreamReader(System.in));
        ecritureEcran = System.out;
        reseau = socket;
        try {
            lectureReseau = new BufferedReader(new InputStreamReader(reseau.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            ecritureReseau = new PrintStream(reseau.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void ecrireEcran(String texte){
        ecritureEcran.println(texte);
    }

    public String lireEcran(){
        String res = "";
        try {
            res = lectureEcran.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }

    public void ecrireReseau(String texte){
        ecritureReseau.println(texte);
    }

    public String lireReseau(){
        String res = "";

        try {
            res = lectureReseau.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return res;
    }
}
